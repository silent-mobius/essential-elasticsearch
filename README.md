<h1 style="color:black">
	<center style="background-color:lightgreen">
		Essential Elasticsearch
	</center>
</h1>

---

# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is elastic search ?
- Who needs it ?
- What ES is capable of ?
- Learn how to manage and query our data
- How to visualize it all.

### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of Data.
- For junior/senior developers who are interested in working on data based systems.
- Experienced ops who need refresher.


---

# Course Topics

- Overview
- Setup ES
- Loading data from remote source of type
- Querying the contents of loaded info
- Analyzing internal
- Visualizing insights


---

# Pre-Requisites

- Linux OS (Debian or RedHat Based) is MUST.
- JSON understanding is MUST.
- Web front end is nice to know.
- Database basics is nice to know.
