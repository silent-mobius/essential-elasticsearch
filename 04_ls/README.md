
--- 
# Essential ELK: LogStash



.footer: Created By Alex M. Schapelle, VaioLabs.io



---

# Working with LogStash

Logstash is a fully customizable data processing engine that ingests data, filters/manipulates it and sends it for save keeping on remote system. There are three blocks of code in a config file usually located at `/etc/logstash/logstash.yml`

- ### Input:
    - Get data in:
        - File
        - Syslog
        - Http
        - Tcp

- ### Output
    - Put data somewhere:
        - Generally Elasticsearch
        - File on disk
        - RabbitMQ or SQS


> `[!]`Note: If you do not define an input or output, Logstash will automatically create an stdin input and stdout output, and it is often has no real connection to real case we are in need of.
---

# Working with LogStash (cont.)

- ### Filter
    - Transformed and manipulated
        - Tagging
        - Structuring 

---

# Working with LogStash (cont.)

Example of out configuration for logStash if `Hello Wolrd` is passed as flat log/txt file:

```json
input { stdin {} }

filter {
    mutate {
        add_field => {"greeting" => "Hello %{message}"}
    }
}

output{ stdout {} }
```

In case of input of out system is `stdin`, then `filter` will tag the log with `greeting` field

---

# LogStash Plugins

Logstash uses the word `plugins` to describe the integration packages for working with various `input` and `output` options.
Plugins are mostly for the filters to transform the data. Logstash comes with over a hundred plugins by default.
Essentially, most of what you need, is already installed. So take a look for yourself, run the Logstash plugins list command, which you can do by giving the full path of `/usr/share/Logstash/bin/logstash-plugins list` or `/usr/bin/logstash-plugins list` (depends on version and type of installation)

---


# LogStash Plugins (cont.)

Due to long list of plugins, we'll only focus on things that can useful for our labs.
Here is an example of `input` for beats plugin:

```json
input {
    beats {
        port => 5044
    }
}
```


---

# Parsing with LogStash filters

### Grok :
-  To absorb or to understand completely
-  Plugin for parsing unstructured data
-  Based on Regex to match patterns in the input

---

# Parsing with LogStash filters (cont.)

below is the example of GROK based `filter` that takes unordered data and reorganizes it in a manner.There are a lot of built-in patterns that will cover most common bits of data like URLs and IP addresses. And of course you can create your own using regular expressions if the defaults aren't quite right
```sh
filter {
    grok {
        match =>
            {
                "message" => 
                "%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}"
            }
    }
}
```

---

# Parsing with LogStash filters (cont.)


the example `output` for this type of  filter might look like this:

```sh
"%{IP:client} %{WORD:method} %{URIPATHPARAM:request} %{NUMBER:bytes} %{NUMBER:duration}"
  51.3.140.18        GET               /login.html        12013              00.51
```

---
# Parsing with LogStash filters (cont.)

Mutate is the other main plugin you're likely to be using to filter data. It's very versatile because it lets you do various transformations of the data. So with this filter block, there's a grok filter to get the data tag correctly, and then a mutate block to operate on the result. 

```sh
grok {
    match => {
        "message" =>
         "%{TIMESTAMP_ISO8601:time} New users created: %{GREEDYDATA:users}"
    }
    mutate {
        split => { "users" => "," }
    }
}
```


---

# Explore Grok Constructor

## What is Grok?

Grok constructor is a free open source web based tool for building filter expressions for LogStash filtering. 

## How to use it ?
The tool comes and OpenSource tool that can be maintained in the company, as a docker container or as web service save in `.war` file.
In the top menu here, click on incremental construction. On this page there's a button to the right here that says random example. Go ahead and click that. And in the URL bar, if it doesn't say example equals two, change that to example two. Or you can just keep clicking the button until you get to example two. We're just using this one so that we're all following through the same example. So now we're ready to build our filter. 

---
# Explore Grok Constructor (cont.)


The first step is to carefully read the log lines. Let me scroll down a little bit. And these are our sample log lines here. What we want to do is get a sense of what the different pieces of data are in our log format. So first we have what looks like an IP address. And then a space, a dash, a space, another dash, and another space. For now, I am going to assume they're visual spacers since we don't have any other examples here, but they could also be optional log entries. In a real log file, you'd want to look around a bit more to be sure. After that, we've got a date set in square brackets. And next is what looks like some web server related log entry data in quotes. There's the verb here, get, and then a path that looks like a URL path, and then an HTTP protocol, HTTP 1.1. And then the last two items are numbers, and without context, I don't really know what they are. Although 200 is a response code from an HTTP server, and so is 302. So, there's a good chance that that's what these represent. Based on context here, I'm not sure what the last item is. Because in this log line, it's just a dash. If this were a real world example, we'd have somewhere to look up what these log lines were and what the different data types are. But I actually think it's a good exercise to just walk through a log entry and try to figure it out yourself. Before we click go to move on, we should look down below here at these check boxes. Now the text here says, "please mark the libraries "of Grok patterns from Logstash, which you want to use." These are the different preset patterns that we could potentially match with. Since this is an HTTP file, we can click the checkbox next to HTTPD. And notice there on the left here. So if your screen has wrapped like mine, you actually want to click this box over here. To see what the patterns actually are, we can click on the link. Now this is a little bit hard to read but these are the different patterns we'll be able to match. One thing to take note of is that some of these patterns have some pretty complex logic. So if we look at this one here under error logs. You'll see that there is a question mark here, some parentheses. If you're not familiar with regular expressions, I know this is a little bit hard to read. But we don't really need to read this, we're actually going to be using this tool, grok constructor to help us work through this. So although you may find yourself wanting to learn the syntax for our purposes right now, we'll be able to use all these built-in patterns. So we'll click the back button to get back to where we were. So we'll make sure that check box is checked, and scroll up to the top of the screen. And then we can click go. And we're ready to start using our tool. And this brings us into our interactive Grok constructor session. In the next lesson, we'll walk our way through constructing the filter using this tool. 

---

# Building Grok 

- [Instructor] We should be ready to build our filter. Make sure you've got example two set in the URL. And down here, you need the check box next to httpd checked. The check boxes are to the left, So be careful of line wrapping here. I actually want to check this one, which correlates to this check box here. Then I'll scroll up and click Go. This is the bit of the UI that I don't find to be very intuitive. It certainly took me a while to figure it out at first. This main text box here is where we're going to be constructing our filter. Slash A just represents the beginning of a string and it's just there so that our log lines don't get mashed up. Below this is the sections that are already matched and the log lines that still need to be matched. So as we build our filter, these things will move over into their already matched category. Below that is a place where you can set a fixed string. Our log lines actually all begin with a pretty long fixed string that's identical, but in our case we know that that's probably going to change. So, we don't want to use fixed string. So we'll keep scrolling down. Now, here in this table, it says Grok expression on the left, and then matches at the start of the rest of the log lines. So this tells us which expressions are available, and how much of the log line they'd potentially match. And the interesting thing here is, there are several that will match the entire line. But we actually only would want to choose one or two of these. Greedydata for example, will match any text at all. So, it won't give us any useful tagging out of our filter. Now, because this is a web weblog, we could use one of these common logs or HTTPD common log. This is actually probably the better choice if we're doing this because the HTTPD common log is the more generic option versus this specific Apache Log. But since we're still learning, let's not pick this easy choice. Let's actually build our own version of the filter that does the similar thing. So we're going to match the first part of our string, which to me looks like an IP address. So I could choose either IP or IPV4. I'm just going to say IP here. Notice there are some other options that would match, but they don't actually make sense. For example, Not Space would match. None of these characters is a space, but it's not going to give us any meaningful tagging of it as an IP address. So let's pick IP. And then we want to scroll back up, and click Continue. Now, if we scroll down, you can see IP has been added to our filter here. And then in the table, that IP has been moved into already matched. Now we can scroll down, in our case we don't know what these dash characters are. These minus characters. And these dots are here actually to represent spaces. We also want to filter that's going to grab this square bracket. So for our purposes, let's just match that as a fixed string. And that correlates to this line here. You could see, we don't want to grab the zero and we want to make sure we include the square bracket. So I'll scroll up and click Continue again. Now we can scroll down again and try to match our date string. So let's see what our options are. So it looks like there's just one good option here, HTTP Date. And that totally matches our date lines. So that must be a standard HTTP date format. So we'll scroll back up and click Continue. And then we want to grab that square bracket and the space. So we'll do this fixed string, closing square bracket and space, and we can click Continue again. You get the idea. By repeating this process, we can build up a basic pattern that matches the data in the log lines. Just to be totally clear if there is a pre-built option that perfectly matches your logs, I'd recommend using that, but this can be a helpful tool for matching custom application logs, or building filters for non-log data. 