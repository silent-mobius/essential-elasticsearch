# Essential ELK



.footer: Created By Alex M. Schapelle, VaioLabs.io

---
# About The Course Itself ?

We'll learn several topics mainly focused on:

- What is elastic search ?
- Who needs it ?
- What ES is capable of ?
- Learn how to manage and query our data
- How to visualize it all.

### Who Is This course for ?

- Junior/senior sysadmins who have no knowledge of Data
- For junior/senior developers who are interested in working on data based systems.
- Experienced ops who need refresher


---

# Course Topics

- Overview  
- Setup ES
- Loading data from remote source of type
- Querying the contents of loaded info
- Analyzing internal
- Visualizing insights


---

# Pre-Requisites

- Linux OS (Debian or RedHat Based) is MUST.
- JSON understanding is MUST.
- Web front end is nice to know.
- Database basics is nice to know.

---
# About Me
<img src="99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science/engineering field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was Cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - Js admirer
    - Golang fallen
    - Rust fan
  
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# History

- 1999 : Lucene 
- 2004 : Compass
- 2012 : ElasticSearch

---

# History (cont.)

1999: Lucene

  - Lucene is a free and open-source search engine software library, originally written in Java by Doug Cutting.
  - Lucene was added to Apache family in 2001 and it became its own top level project in 2005
  - Lucene included in it other top level projects such as [Nutch](https://en.wikipedia.org/wiki/Apache_Nutch) which you may know as [HDFS](https://en.wikipedia.org/wiki/Distributed_file_system_for_cloud)
  - The nature of Lucene was to help existing search engines to index the data that they were inhaling from the internet and provided reasonable ways of retrieving that data based on `fuzzy` matching
    - Example: `black kitten` search would find all possible, pictures, documents, videos and so on, related to `black kitten`. 
  - The real innovation here was that Lucene could extract all text from any type of content and then retrieve that data(text) with knowing those exact terms.

---
# History (cont.)

2004: Compass

  - Shay Banon create software called `Compass`
    - It was build on top of Lucene and provided, essentially the same services
    -  Compass idea was to be more scalable search solution while using common web transfer protocol
    -  While thinking about the third version of Compass he realized that it would be necessary to rewrite big parts of Compass to "create a scalable search solution".
       - So the solution that was created used a common interface, JSON over HTTP, suitable for programming languages other than Java as well
       - Thus Elastic Search was born

---

# History (cont.)

 2012: Elastic

  - Elastic NV was founded in 2012 to provide commercial services and products around Elasticsearch and related software.
  -  In March 2015, the company Elasticsearch changed their name to Elastic. 

---

# Elastic Company

 Elastic the company provide several products:

  - `ELK Stack`:
    - They distributed as OSS based on Apache Licence, but due issues with AWS, they dicided to re-license with something called (Server-Side Public)[https://en.wikipedia.org/wiki/Server_Side_Public_License] license.
  - `X-Pack` : Other software of Elastics' stack that provide security, monitoring and alerting
    - this is paid service and require license.

---

#  ELK

So, what is the ELK Stack? "ELK" is the acronym for three open source projects:

- Elasticsearch
- Logstash
- Kibana

Elasticsearch is a search and analytics engine. 
Logstash is a server‑side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a "stash" like Elasticsearch.
Kibana lets users visualize data with charts and graphs in Elasticsearch.

The Elastic Stack is the next evolution of the ELK Stack. 

---

# Elasticsearch

The open source, distributed, RESTful, JSON-based search engine. Easy to use, scalable and flexible, it earned hyper-popularity among users and a company formed around it, you know, for search. 

---

# Logstash and Kibana

A search engine at heart, users started using Elasticsearch for logs and wanted to easily ingest and visualize them. Enter Logstash, the powerful ingest pipeline, and Kibana, the flexible visualization tool.

---
# Beat on ELK

In 2015, we introduced a family of lightweight, single-purpose data shippers into the ELK Stack equation. We called them Beats.

