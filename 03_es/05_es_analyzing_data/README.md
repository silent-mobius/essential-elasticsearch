

---

# Basic Aggregation

- What is aggregation?
    - Combination of buckets and metrics of data.
- How to create aggregation using ES api ?
    - `aggs` query

```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search \
    -d '{"size":0,"aggs":{"states":{"terms":{"field": "state.keyword"}}}}'
```
we can combine several aggregations:

```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search -d '{"size":0
    ,"aggs":
        {"states":
            {"terms":
                {"field": "state.keyword"}, 
            "aggs": 
                {"avg_bal":
                    { "avg":
                         {"field": "balance"}
                    }
                }
            }
        }
    }'
```

---

# Filtering Aggregation

- Count of Accounts by State
- Must be keyword field
- This is the equivalent of using match_all
- `Aggs` work in the context of the query, so apply a filter like normal
- You can also filter on terms
- Lets add a metric back in
- You can also just filter the results
- Look at state avg and global average


---

# Percentiles and Histograms

- Look at the percentiles for the balances
- Can also calculate High Dynamic Range (HDR) Histogram
- We can use the percentile ranks agg for checking a individual values
- Similarly we can create a histogram

