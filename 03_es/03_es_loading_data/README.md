
---

# Essential ELK: ElasticSearch



.footer: Created By Alex M. Schapelle, VaioLabs.io

---

# Bulk Loading Data

## Bulk API

When passing data to ES, we can use `_bulk` api request. In case of curl, we also verify type of json, by reading several lines of it, in case it's type is different either `json` or `x-ndjson` <!-- newline delimiter json -->
also `--data-binary` flag needs to be passed.
In case we are using `Kibana Dev-Tool` we can just use `_bulk` api and paste the data to UI.
So to sum it up:

- `/_bulk`
- new-line JSON
- index, Create, Delete, Update
- `curl --data-binary`
<!-- show example and then practice it 
1. create reqs file (data file) : copy from exercise file
2. curl -s -H "Content-Type: application/x-ndjson" -XPOST localhost:9200/_bulk --data-binary "@reqs";echo -->

---

# Lab 1 

- Use curl command to pass bulk data injection to ES of `accounts.json`.
- Validate the data with in the indices.

---

# Lab 2

- Check list of indices in ES.
- Check what does bank has in it.

---

# Setting Data Types

- In ELK Stack there variety of data types that can be used and also set up.

| Data types  | Description | Example |
|  ---        | ---         | ---     |
| Core        | Int, Float, String, boolean, binary| |
| Complex     | Array      | |
| Geo         | Geo shape, Geo Point | |
| Specialized | ip, autocomplete, tokens| |


---

# Setting Data Types (cont.)

- As an example we'll create mapping for our log data, that you can find at [exercise files](99_misc/exercise%20files/data/logs.jsonl)
  - Mappings: initial structure that keeps data sorted (Schema)
  - It is not crucial, because usually ES can create it on the fly, but it is good practice to create them.

- Lets create mappings:
```sh  
curl -X PUT localhost:9200/logstash-2015.05.18 -d '{ "mappings": { "log": { "properties": { "geo": { "properties": { "coordinates": { "type": "geo_point"}}}}}}'
# and another one:
curl -X PUT localhost:9200/logstash-2015.05.19 -d '{ "mappings": { "log": { "properties": { "geo": { "properties": { "coordinates": { "type": "geo_point"}}}}}}' 
# and last one:
curl -X PUT localhost:9200/logstash-2015.05.20 -d '{ "mappings": { "log": { "properties": { "geo": { "properties": { "coordinates": { "type": "geo_point"}}}}}}'
```

> `[!]`Note: the difference is only the dates that store mappings from logs.jsonl

> `[!]`Note: it is possible that in ES v8+ mapping will be removed

---

# Setting Data Types (cont.)

- We'll import data from local `logs.jsonl` file:
  - `curl -H 'Content-Type: application/x-ndjson' -XPOST 'localhost:9200/_bulk?pretty' --data-binary @logs.jsonl`
  
- Lets check if we have data here:
  - `curl localhost:9200/_cat/indices/logstash-*`

---

# Lab 3

- Use `shakespeare.json` to load to  ES and validate its indices.
  - Create mappings for the json
    - Read the json and try to determine what type of datas you'll need insert in your mappings
  <!-- - `curl -X PUT "localhost:9200/shakespeare?pretty" -H 'Content-Type: application/json' -d'{"mappings": {"properties": {"speaker": {"type": "keyword"},"play_name": {"type": "keyword"}, "line_id": {"type": "integer"},"speech_number": {"type": "integer"}}}}'` -->
    - Load that mapping to ES
  - Insert `shakespeare.json` to ES to mappings inserted
  <!-- - `curl -H 'Content-Type: application/x-ndjson' -X POST 'localhost:9200/shakespeare/_bulk?pretty' --data-binary @shakespeare.json` -->
  - Validate that data is in the ES
  <!-- `- curl -X GET "localhost:9200/_cat/indices?v&pretty"` -->
