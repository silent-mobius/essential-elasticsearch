

---

# Simple Query

One of part about ElasticSearch is that lets us work with API literally to do everything. . Let's start here just by running a query to return everything.

```sh
curl localhost:9200/bank/account/_search # equivalent to print all data.
```
---
# Simple Query (cont.)

But getting is all is not what we always want, thus we'll cover simple queries:

- `match`
- `bool`
    - `must`
    - `must_not`
    - `should`

---

# Simple Query (cont.)

Checking one specific value can be checked with `match` condition:

```sh
curl -X GET -H 'Content-Type: application/json'  bank/account/_search \
{
  "query": {
    "match": {
      "state": "CA"
    }
  }
}

```

---
# Simple Query (cont.)

When more then one condition is required we can boolean condition, thus adding more `match` conditions as shown below

```sh
curl -X GET -H 'Content-Type: application/json'  bank/account/_search \
{
  "query": {
    "bool": {
      "must": [
        { "match": {"state": "CA"} },
        { "match": {"employer": "Techade"}}
      ]\
    }\
  }\
}

```
---
# Simple Query (cont.)

In case of negation,<!-- negative condition-->  we can use `must_not` boolean operator that will enable us to get reversed query answers

```sh
curl -X GET -H 'Content-Type: application/json'  bank/account/_search \
{
  "query": {
    "bool": {
      "must_not": [
        { "match": {"state": "CA"} },
        { "match": {"employer": "Techade"}}
      ]
    }
  }
}

```
---

# Simple Query (cont.)

While strict conditioning with `must`  and `must_not` may be handy, it is also a possibility to get query data that enables us `boost` specific values from others. essentially is you have boost on specific value it will still be highlighted by ES, even if it is not match by previous or after condition.

```sh
curl -X GET -H 'Content-Type: application/json'  bank/account/_search \
{
  "query": {
    "bool": {
      "should": [
        { "match": {"state": "CA"} },
        { "match": {"lastname": {"query": "Smith","boost": 3}}
        }
      ]
    }
  }
}

```

---

# Term-level Queries

In cases where we need to get query based on detailed key or value we can use `term` based search. this type of query is mainly for key words and numeric value. where as `match` is mostly text fields.
Example
```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search
{
  "query": {
    "term": {
      "account_number": 516
    }
  }
}
```

---
# Term-level Queries (cont.)

In complex cases of data query where, we have more then one `term`, then we can combine them with `[]`
for Example:

```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search
{
  "query": {
    "terms": {
      "account_number": [516,851]
    }
  }
}
```

---

# Range Query

There also cases in which we need provide `range` term for querying the data that we are injecting.
Ranges can be range queries:

- gte: Greater-than or equal to
- gt: Greater-than
- lte: Less-than or equal to
- lt: Less-than

```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search
{
  "query": {
    "range": {
      "account_number": {
        "gte": 516,
        "lte": 851,
        "boost": 2 
      }
    }
  }
}
# we can boost it as well
```


---
# Range Query (cont.)

Essentially range can be implemented in any type for query
```sh
curl -H 'Content-Type: application/json' localhost:9200/bank/account/_search
{
  "query": {
    "range": {
      "age": {
        "gt": 35
      }
    }
  }
}
```
---