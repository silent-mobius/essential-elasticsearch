
--- 

# Essential ELK: Beats



.footer: Created By Alex M. Schapelle, VaioLabs.io


---
# Beats 
## What are beats ?

As a short history lesson, in centralized logging, a data pipeline consists of three main stages: 
- aggregation
- processing
- storage.

In the ELK Stack, the first two stages were traditionally the responsibility of `Logstash`, the stack’s work horse.

Executing these tasks comes at a cost. Due to inherent issues related to how Logstash was designed, performance issues became a frequent occurrence, especially with complicated pipelines that require a large amount of processing. 

As such Beats were created to be a collection of lightweight (resource efficient, no dependencies, small) and open source log shippers that act as agents installed on the different servers in your infrastructure for collecting logs or metrics.

---

# Beats (cont.)

- Heart beat: is the beat for tracking uptime of your systems. 
- File beat: is for files, usually log files. 
- Metric beat: is for shipping system metrics. 
- Packet beat: for network data.
- The list goes on ...
 
> `[!]`Note: we're focus on filebeat, yet usage of other beats works the same way. 

---

# Getting Beats

### Where to get the beats ?

Installing the various beats is super simple. Most likely, you’ve already set up one of the other components in the ELK Stack, so you should be familiar with the commands.
Just in case, here is a full example of installing a beat from scratch on an Debian based host.

```sh
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | \
    sudo apt-key add -

echo "deb https://artifacts.elastic.co/packages/6.x-prerelease/apt stable main" | \
    sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
```
To install your beat, use:

```sh
apt-get update
apt-get install -y filebeat
```

---

# Getting Beats (cont.)

In cases where, the repository addition can not be implemented, we can simple download the package from elastic.co site, and install it manually as follow: 

```sh
curl -L -O \
https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-7.6.2-amd64.deb
dpkg -i filebeat-7.6.2-amd64.deb

```

> `[!]`Note: the version might be different by the time you read this material. Please check with elastic.co

---

# Getting Beats (cont.)

In use case, where we are using containers it is also possible to use specific beat container for monitoring purposes.

To get such a container, simple pull from docker registry the beat container that we are interested in, and run it.

```sh
docker pull docker.elastic.co/beats/filebeat:7.6.2

```

> `[!]`Note: the version might be different by the time you read this material. Please check with elastic.co


---
# Set up beats on environment

Once the beats is installed, we can configure it to ship the files, logs, metrics, networks, and/or heart.

To do so, we need to configure the beat configuration file and redirect it towards Elasticsearch and Kibana.

```yml
output.elasticsearch:
  hosts: ["elasticsearch_address_or_domain_name:9200"]  
  # default port if port redirection is not configured.
  username: "elastic" 
  #only if configured 
  password: "<password>"
#only if configured 
setup.kibana:
  host: "kibana_address_or_domain_name:5601" 
  # default port if port redirection is not configured.

```
---
# Set up beats on environment

For each of the beats there are additional configuration that needs configured. In case of filebeat:


```yaml

filebeat.prospectors:
- type: log
  enabled: true
  paths:
    - /var/log/*.log
  fields:
    app_id: service-a
    env: dev
output.logstash:
  hosts: ["localhost:5044"]

```
> `[!]`Note: beats can also redirect data to logstash for additional filtering, as shown in example


---

# Configuration best practices

Each beat contains its own unique configuration file and configuration settings, and therefore requires its own set of instructions. Still, there are some common configuration best practices that can be outlined here to provide a solid general understanding.

- Some Beats, such as filebeat, include full example configuration files (e.g, /etc/filebeat/filebeat.full.yml). These files include long lists all the available configuration options.
- YAML files are extremely sensitive. DO NOT use tabs when indenting your lines - only spaces. YAML configuration files for Beats are mostly built the same way, using two spaces for indentation.
- Use a text editor (I use vscode) to edit the file.
- Sections are separated by a long line of # (hash) symbols - as much as this sounds funny - don’t mistake these for commented out configuration lines.
- The `-` (dash) character is used for defining new elements - be sure to preserve their indentations and the hierarchies between sub-constructs.

---

# Lab

- Add another instance of nginx container to `elk-stack` environment.
- Connect to nginx container and install metric beat.
- Configure to metric beat to send data to elasticsearch and kibana accordingly.
  - Use `docker` module for metric beat
  - Monitor "container", "cpu", "diskio", "healthcheck", "info", "memory" and "network.
- Create index template in kibana for new container.
- Setup visualization of nginx load on the system.
  - We'll need to simulate the load on the container with simple shell/python loop connecting to nginx container.
- Add data to existing dashboard.

<!-- metricbeat.modules:
- module: docker
  metricsets: ["container", "cpu", "diskio", "healthcheck", "info", "memory", "network"]
  hosts: ["unix:///var/run/docker.sock"]
  period: 10s
fields:
  env: dev
output.elasticsearch:
  hosts: ["elasticsearch:9200"] -->