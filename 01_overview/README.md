
---

# Essential ELK: Overview



.footer: Created By Alex M. Schapelle, VaioLabs.io

---

# Elastic Stack Overview

---

# Elastic Stack Overview (cont.)

### ELK-Stack OSS

<img src="99_misc/.img/ELK3.png" alt="drawing" style="float:center;width:700px;">


---

# Elastic Stack Overview (cont.)

### Full list of ELK products
  
<img src="99_misc/.img/ELK9.png" alt="drawing" style="float:center;width:700px;">


---

# Elastic Stack Overview (cont.)
### There are also cloud option of ELK-Stack
 
 - AWS provides ELK-Stack
 - logz.io

<img src="99_misc/.img/ELK_full.png" alt="drawing" style="float:center;width:700px;">



---

# Elastic Stack Overview (cont.)

### Reasons why to use ELK-Stack

<img src="99_misc/.img/ben3.png" alt="drawing" style="float:center;width:700px;">

---

# ES Use Cases

The ELK Stack is most commonly used as a log analytics tool. Its popularity lies in the fact that it provides a reliable and relatively scalable way to aggregate data from multiple sources, store it and analyze it. 

- Security/Log
- Marketing/BI
- Operations
- Search

---

# Elastic Search Concepts

- Cluster 
    - Collection of nodes
- Node
    - Individual server part of cluster
- Index
    - Indices are logical partitions of documents and can be compared to a database in the world of relational databases shared in distributed manner over the cluster
- Document
    - Documents are JSON objects that are stored within an Elasticsearch index and are considered the base unit of storage. In the world of relational databases, documents can be compared to a row in a table
- Type
    - Elasticsearch types are used within documents to subdivide similar types of data wherein each type represents a unique class of documents. Types consist of a name and a mapping and are used by adding the `_type` field

---
# Elastic Search Concepts (cont.)

- Mapping
    - Like a schema in the world of relational databases, mapping defines the different types that reside within an index. It defines the fields for documents of a specific type `—` the data type (such as string and integer) and how the fields should be indexed and stored in Elasticsearch. A mapping can be defined explicitly or generated automatically when a document is indexed using templates
- Shard
    - Index size is a common cause of Elasticsearch crashes. One way to counter this problem is to split up indices horizontally into pieces called shards. This allows you to distribute operations across shards and nodes to improve performance

---

# Elastic Search Concepts (cont.)

- Replica
    - To allow you to easily recover from system failures such as unexpected downtime or network issues, Elasticsearch allows users to make copies of shards called replicas
- Elasticsearch Queries
    - Elasticsearch is built on top of Apache Lucene and exposes Lucene’s query syntax.
- Fields
    - You might be looking for events where a specific field contains certain terms. You specify that as follows : `name:”Ned Stark”`

---

# Elastic Search Concepts (cont.)


- Boolean Operators
    - As with most computer languages, Elasticsearch supports the AND, OR, and NOT operators:
      - jack NOT jill — Will return events that contain both jack and jill
      - ahab OR moby — Will return events that contain ahab but not moby
      - tom AND jerry — Will return events that contain tom or jerry, or both

---

# Elastic Search Concepts (cont.)

- Ranges
    - You can search for fields within a specific range, using square brackets for inclusive range searches and curly braces for exclusive range searches:
      - age:[3 TO 10] — Will return events with age between 3 and 10
      - price:{100 TO 400} — Will return events with prices between 101 and 399
      - name:[Adam TO Ziggy] — Will return names between and including Adam and Ziggy

---

# Elastic Search Concepts (cont.)

  - Wildcards, Regex and Fuzzy Searching
    - A search would not be a search without the wildcards. You can use the * character for multiple character wildcards or the ? character for single character wildcards.

  - URI Search
    - The easiest way to search your Elasticsearch cluster is through URI search. You can pass a simple query to Elasticsearch using the q query parameter. The following query will search your whole cluster for documents with a name field equal to “travis”:
      - `curl “localhost:9200/_search?q=name:travis”`
  
---

# Elastic Search Concepts (cont.)

- Elasticsearch REST API
  - One of the great things about Elasticsearch is its extensive REST API which allows you to integrate, manage and query the indexed data in countless different ways. Examples of using this API to integrate with Elasticsearch data are abundant, spanning different companies and use cases

---

# Elastic Search Concepts (cont.)

<img src="99_misc/.img/cluster.png" alt="drawing" style="float:center;width:700px;">

---

# Chapter Quiz

## What is the L in the ELK Stack?

- Long Tail
- LogSort
- LogStash   <!-- answer-->
- Lucene

---

# Chapter Quiz (cont.)

## Which term describes a collection of nodes?

- Cluster  <!-- answer-->
- Shard
- Index
- Node Cluster
