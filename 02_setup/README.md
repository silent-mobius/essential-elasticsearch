
---

# Essential ELK: Setup



.footer: Created By Alex M. Schapelle, VaioLabs.io

---

# Installing Pre-Requisites

- Linux OS
- VirtualBox/VMWare/KVM
- Vagrant/Docker+Docker-compose
- Automated install of ELK Stack
- Java 8 (at least)

---

# Installing Pre-Requisites (cont.)

- Install VirtualBox
- Install Vagrant
- Run Vagrant Playbook

OR

- Install Docker and Docker-compose
- Run Docker-compose playbook

> `[!]`Note: due to intense memory use, in case of docker, we need to increase Linux kernel memory usage: `sudo sysctl -w vm.max_map_count=262144`. Please RTFM.

---

# Running Elastic Search locally
During next class we'll going to :

- Run cluster locally
- Query your cluster locally (curl)

---

# Exploring ES cluster

Table below presents example of table to be created in ES

| <REST Verb\>|  <Index\>| <Type\> | <Id\> |
| ---       |  ---   |  ---   | --- |
| PUT         | sales   | order   |246 |
| GET         | sales   | order   |357 |
| DELETE      | sales   |         |    |

---
# REST API

  - Admin functions
  - Cluster health
  - Crud operation
  - Search capability

---
# Admin Functions

ES is a interactive system, so we can query it for anything. In case of error we'll be notified with HTTP return codes, such as 400,404,405 and so on. Example of error:

```sh
curl localhost:9200/_?
```
The output should be 400 return code, mainly notifying that requested query is invalid.
To learn ES syntax we can use curl for getting acknowledged with RESTApi.

```sh
curl localhost:9200/_cat? # will print all possible subcommands with  `_cat`
```

> `[!]`Note : `_cat` is a short for Compact and Aligned Text. it is used for human readability2

---

# Admin Functions (cont.)

Explanations can be found with `?help` for the RESTApi query
```sh
curl localhost:9200/_cat/health?help
```
in case of stacked output we can add `?v` make the printout verbose, it gives information perspective.
`?pretty` is also good option for human readable format

```sh
curl localhost:9200/_cat/health?v
curl localhost:9200/_nodes/os?pretty
```

> `[!]`Note: complete list of RESTApi description can be found  [here](https://www.elastic.co/guide/en/elasticsearch/reference/current/rest-apis.html)
> `[!]`Note: if `?v` and `?pretty` are giving error it means that they are not applicable, thus we need to check the documentation

---
# Admin Functions (cont.)
Examples provided before can be used to get info in regards to cluster, nodes and node status, as well can change in memory configuration, which we will not cover.
Yet, addition data that can be useful is in regards to indices that are created:

```sh
curl localhost:9200/_cat/indices
curl localhost:9200/_cat/indices?v
curl localhost:9200/_cat/indices?help
```

---
# Cluster Health

After setting the ES cluster it is good idea to verify its functionality, thus checking the clutter, nodes, configuration and so on is some what mandatory. below you have several examples of these `health` checks

```sh
curl  localhost:9200/_cat/health?v&pretty # pretty  ad the end will provide human readable format
                                                 # v stands for verbose  
```
---

# Cluster Health (cont.)

In the same manner we can check all our nodes to print out  status of our nodes.
```sh
curl  localhost:9200/_cat/nodes?v&pretty
```

In case there is a need to check specific node in the cluster we can require a cluster name and query its status
```sh
curl localhost:9200/_cat/nodes/process?pretty
```
---

# Cluster Health (cont.)


once the installation is done, usually it is good idea to check on the statistics of our cluster by using  `stats`

```sh
curl localhost:9200/_cluster/stats?pretty
```

---

# CRUD Operations

CRUD is acronym for:

- Create
- Read
- Update
- Delete

CRUD operations enable us to communicate with ES and perform all the tasks described above, but with RESTApi communication, via http protocol, thus converting all the operation to what we call HTTP commands:

- PUT to create
- GET to read
- POST to update
- DELETE 
 lets see it in action

---
# CRUD Operations (cont.)

Creating `index` <!-- a database --> with name `sales`

```sh
curl -H 'Content-Type: application/json' -X  PUT /sales
```

As mentioned, ES is schemaless, thus it enables us to create mapping on the go, even if we never went for it.

---

# CRUD Operations (cont.)

We can update with POST our `index` , by passing to it a `_type` and values to that type.

```sh
curl  -H "Content-Type: application/json" -X POST  "localhost:9200/sales/order/246" -d '{ "orderID":"246","orderAmount":"321"}' 
```
<!-- curl  -H "Content-Type: application/json" -X POST  "localhost:9200/sales/order/246" -d '{ "orderID":"246","orderAmount":"321"}'  -->

---
# CRUD Operations (cont.)

As to getting the  existing `index` with its descriptions:

```sh
curl  -H "Content-Type: application/json" -X GET localhost:9200/sales # GET is optional, due to it being default behavior of curl
```
---

# CRUD Operations (cont.)

To sum it up, we can delete the `index` by passing DELETE command to ES

```sh
curl  -H "Content-Type: application/json" -X DELETE /sales
```

---
# Lab

- Setup your cluster based on Vagrant/Docker files.
<!-- - vagrant up  should install all but might need to fix som configs -->
- Check with curl the health of your cluster.
<!-- - curl  localhost:9200/_cat/health?v -->

---
# Lab

- Create new index named manga
<!-- - curl -X PUT localhost:9200/sales -->
- Create new type naruto in  index.
<!-- - curl  -H "Content-Type: application/json" -X PUT  "localhost:9200/sales/order/246" -d '{ "orderID":"246","orderAmount":"321"}' -->
- Insert next details to type:
    - name: uzumaki naruto
    - age: 28
    - name: huyga hinata
    - age: 26
- Verify these details
- Delete `naruto`

---

# Quiz:

Which command retrieves a document from the Index?

- read
- get
- retrieve
- scan
<!-- - get -->
