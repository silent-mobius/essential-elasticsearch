#!/usr/bin/env python3

import sys
import time
import random
import requests
import logging
import logging.config
from pythonjsonlogger import jsonlogger
from datetime import datetime


path_list = ['/', '/bash', '/slides', '/python', '/yaml', '/proc', '/sys']
random_path = random.choice(path_list)

class ElkJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(ElkJsonFormatter, self).add_fields(log_record, record, message_dict)
        log_record['@timestamp'] = datetime.now().isoformat()
        log_record['level'] = record.levelname
        log_record['logger'] = record.name

logging.config.fileConfig('logging.conf')
logger = logging.getLogger("MainLogger")

count = 0
while True:
    if (count == 1000000):
        sys.exit()
    else:
        req = requests.get(f'http://vaiolabs.io{random_path}')
        time.sleep(1)
        logging.info('Application running!')
        logging.info(req)
        time.sleep(1)
        count = count + 1